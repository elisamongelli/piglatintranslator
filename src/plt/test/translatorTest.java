package plt.test;

import static org.junit.Assert.*;

import org.junit.Test;

import plt.*;

public class translatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}

	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithASingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonants() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMultipleWords() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithCompositeWords() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMultipleWordsAndCompositeWords() {
		String inputPhrase = "hello world well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithExclamationMark() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithGenericPunctuationCharacter() {
		String inputPhrase = "hello! i'm elisa, i'm twenty-one:years old.";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay! iyay'may elisayay, iyay'may entytway-oneyay:earsyay olday.", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithPunctuationChar() {
		String inputPhrase = ";hello! i'm elisa, i'm twenty-one:years old.";
		Translator translator = new Translator(inputPhrase);
		assertEquals(";ellohay! iyay'may elisayay, iyay'may entytway-oneyay:earsyay olday.", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithStar() {
		String inputPhrase = ";hello! i'm elisa, i'm*twenty-one:years old.";
		Translator translator = new Translator(inputPhrase);
		assertEquals(";ellohay! iyay'may elisayay, iyay'may*entytway-oneyay:earsyay olday.", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithStar() {
		String inputPhrase = "*hello! i'm;elisa, i'm*twenty-one:years old.";
		Translator translator = new Translator(inputPhrase);
		assertEquals("*ellohay! iyay'may;elisayay, iyay'may*entytway-oneyay:earsyay olday.", translator.translate());
	}
}
