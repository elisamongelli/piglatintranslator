package plt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Translator {
	
	private String phrase;
	public final static String NIL = "nil";

	public Translator(String inputPhrase) {
		this.phrase = inputPhrase;
	}

	public String getPhrase() {
		return this.phrase;
	}

	public String translate() {
		
		if ("".equals(this.phrase))
			return NIL;
		
		
		ArrayList<Character> specialChars = new ArrayList<Character>();
		ArrayList<Integer> starIndices = new ArrayList<Integer>();	// indices of stars in phrase, but relative to specialChars indices
		
		boolean containsChar = false;
		int relativeStarIndex = 0;
		
		StringBuilder tempPhrase = new StringBuilder(this.phrase);	// this.phrase with stars instead of punctuation chars
		
		for(int i = 0; i < this.phrase.length(); i++) {
			if(isPunctuationChar(this.phrase.charAt(i)) || this.phrase.charAt(i) == '-') {
				specialChars.add(this.phrase.charAt(i));
				tempPhrase.setCharAt(i, '*');
				
				containsChar = true;
				relativeStarIndex++;
			}
			else if(this.phrase.charAt(i) == '*') {
				starIndices.add(relativeStarIndex);
			}
		}
		this.phrase = tempPhrase.toString();
		
		ArrayList<String> words = new ArrayList<String>(Arrays.asList(this.phrase.split(" ")));
		ArrayList<String> newWords = new ArrayList<String>();

		for (int k = 0; k < words.size(); k++) {
			
			this.phrase = words.get(k);
			
			boolean phraseStartsWithStar = false;	// star can be a special character or a star in the original phrase
			if(this.phrase.startsWith("*")) {
				phraseStartsWithStar = true;
				this.phrase = this.phrase.substring(1);
			}
			
			boolean phraseEndsWithStar = false;		// star can be a special character or a star in the original phrase
			if(this.phrase.endsWith("*"))
				phraseEndsWithStar = true;
			
			
			ArrayList<String> compositeWord = new ArrayList<String>(Arrays.asList(this.phrase.split("\\*")));
			ArrayList<String> newCompositeWords = new ArrayList<String>();
			
			for (int p = 0; p < compositeWord.size(); p++) {
				String tempWord = compositeWord.get(p);
				
				if(startsWithVowel(tempWord)) {
					if (tempWord.endsWith("y")) {
						newCompositeWords.add(tempWord + "nay");
					}
					else if (endsWithVowel(tempWord)) {
						newCompositeWords.add(tempWord + "yay");
					}
					else if (!endsWithVowel(tempWord)) {
						newCompositeWords.add(tempWord + "ay");
					}
				}
				else {
					char[] startingConsonants = new char[tempWord.length()];
					char[] arrayPhrase = new char[tempWord.length()-1];
					
					int i, q = 0;
					boolean flag = false;
					for(i = 0; i < tempWord.length(); i++) {
						if(!isVowel(tempWord.charAt(i)) && !flag) {
							startingConsonants[i] = tempWord.charAt(i);
							continue;
						}
						flag = true;
						arrayPhrase[q++] = tempWord.charAt(i);
					}
					
					newCompositeWords.add(String.valueOf(arrayPhrase).trim() + String.valueOf(startingConsonants).trim() + "ay");
				}
			}
			
			String newCompositeWord = "";
			if(phraseStartsWithStar)
				newCompositeWord += "*";
			for(int p = 0; p < newCompositeWords.size(); p++) {
				newCompositeWord += newCompositeWords.get(p);
				if (p < newCompositeWords.size()-1 || phraseEndsWithStar) {
					newCompositeWord += "*";
				}
			}
			newWords.add(newCompositeWord);
		}
		
		String newPhrase = "";
		for(int i = 0; i < newWords.size(); i++) {
			newPhrase += newWords.get(i);
			if (i < newWords.size()-1) {
				newPhrase += " ";
			}
		}
		int indexSpecialChar = 0;
		boolean flag = false;
		StringBuilder tempNewPhrase = new StringBuilder(newPhrase);
		for(int i = 0; i < newPhrase.length(); i++) {
			if(newPhrase.charAt(i) == '*') {
				if(starIndices.contains(indexSpecialChar) && flag == false) {
					flag = true;
					continue;
				}
				tempNewPhrase.setCharAt(i, specialChars.get(indexSpecialChar));
				indexSpecialChar++;
				flag = false;
			}
		}
		newPhrase = tempNewPhrase.toString();
		return newPhrase;
	}
	
	public boolean startsWithVowel(String phrase) {
		return (phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || 
				phrase.startsWith("o") || phrase.startsWith("u"));
	}
	
	public boolean endsWithVowel(String phrase) {
		return (phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || 
				phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	public boolean isVowel(char ch) {
		return (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u');
	}
	
	public boolean isPunctuationChar(char ch) {
		return (ch == '.' || ch == ',' || ch == ';' || ch == ':' || ch == '?' || ch == '!' || ch == '\'' || ch == '(' || ch == ')');
	}
	
}
